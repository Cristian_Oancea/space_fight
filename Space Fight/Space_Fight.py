############################################################################################
# To start the game press SPACE                                                           ##
# Left click on a planet selects it, SHIFT+Left click selects all your planets            ##
# Mouse scroll modifies the % of ships you want to send from the selected planets         ##
# Before running th program you might need to install some modules: random, numpy, pillow ##
############################################################################################
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from OpenGL.GLUT import fonts
import datetime
import math
import GameObjects as GO
import random, numpy
import PIL.Image        #need to install Pillow not PIL in Python



#try:
#except ZeroDivisionError:
firstFrame = 0                          #needed to skip the first frame for AI
texture = []                            #holds all the textures
texture.extend([None]*6)
clock = pygame.time.Clock()
width = 1280                            #window width   (can be changed to other resolution values)
height = 720                            #window height  (can be changed to other resolution values)
mouseX = 1                              #used to store X position of mouse (pixels)
mouseY = 1                              #used to store Y position of mouse (pixels)
button = 0                              #used to store an int that tells which mouse button was used (1-LMB 2-MMB 3-RMB 4-WheelUp 5-WheelDown 6 and 7-Side Buttons)
noPlanets = 30                          #number of planets
noShips = 200                           #maximum number of ships     (must change with dynamic memory) !!!<======================================
p = []                                  #declare list that holds objects planets
p.extend([None]*noPlanets)              #extend list
s = []                                  #declare list that holds objects ships
s.extend([None]*noShips)                #extend list
pl = []                                 #declare players list
pl.extend([None]*5)                     #extent list to 5 since player 0 is neutral and we have 4 players maximum
fps = 1                                 #declare 1 at start of program to prevent divide by zero
run = False                             #Used to loop the program on key pressed
play = False                            #used to check wheter or not a song is already playing, True if it is
spacekey = True                         #used to check if space key is pressed
select = 0                              #used to check if we select a planet or all
send = False                            #used to check if we send ships from one planet to another
amount = 100                            #stores how many troops to send with ship (in %)
#song='Data\\mayday.ogg'                #A string that holds the current song's path
temp=datetime.datetime.now()           #Used to initialize fpstime as gobal (I suck at this)
fpstime=datetime.datetime.now()-temp   #Same as above, sucky suck !

zoom_val = -5                   #how much we zoom on Z axis
shipsAlive = []                 #used to check if ship should exists   (pretty sure there is a better way than this)
shipsAlive.extend([None]*noShips)   
shipSpeed1 = 0.1
shipSpeed = []                  #used to give speed to ships
shipSpeed.extend([None]*noShips)   
for i in range(100):
    shipsAlive[i] = False       
    shipSpeed[i] = 0.1



#Not using this anymore but i'll keep it for now
def getFps():
    global temp, fps, fpstime, clock
    try:
        fpstime=datetime.datetime.now()-temp
        fps=1000000/fpstime.microseconds
    except ZeroDivisionError:
        fps = 1
    temp=datetime.datetime.now()
    return fps

######################################################## Get mouse coordinates in pixels and tranform in gl units ######################################################################
def getMouseCoord():
    global mouseX, mouseY, zoom_val, height, width
    mouse_pos = (mouseX, mouseY)    #mouse position in pixels based on resolution (window related)
    ortho_2d = (zoom_val*width/height, -zoom_val*width/height, -zoom_val, +zoom_val)     #ugly fix (if you break this you'll fix it!!)

    #First, calculate the "normalized" mouse coordinates by dividing by window_size
    mouse_norm_x = mouse_pos[0] / width
    mouse_norm_y = mouse_pos[1] / height

    #Now map those coordinates to your orthographic projection range
    mouse_ortho_x = (mouse_norm_x * (ortho_2d[1] - ortho_2d[0])) + ortho_2d[0]
    mouse_ortho_y = (mouse_norm_y * (ortho_2d[3] - ortho_2d[2])) + ortho_2d[2]
    #mouse_ortho = (mouse_ortho_x, mouse_ortho_y)
    return mouse_ortho_x, mouse_ortho_y

##################################################################### Text rendering function ###########################################################################################
#for details check https://www.pygame.org/docs/ref/font.html#pygame.font.Font
def drawText(x, y, textString):     
    global zoom_val, font                 #zoom
    #font = pygame.font.Font (None, 30)  #create font
    textSurface = font.render(textString, True, (255,255,255,255), (0,0,0,255))     #create surface
    textData = pygame.image.tostring(textSurface, "RGBA", True)     
    glRasterPos3d(x, y, 0.01)     #position to render text
    #TODO: Use glTexSubImage2D instead of glDrawPixels !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)    #draw the text

################################################################## Loads textures when called #########################################################################################
def loadTexture(name):

    img = PIL.Image.open(name).transpose( PIL.Image.FLIP_TOP_BOTTOM ) # .jpg, .bmp, etc. also work
    img_data = numpy.array(list(img.getdata()), numpy.int8)

    id = glGenTextures(1)
    glPixelStorei(GL_UNPACK_ALIGNMENT,1)
    glBindTexture(GL_TEXTURE_2D, id)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.size[0], img.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, img_data)
    return id


##################################################################### Create and draw object, also calculate interactions ###############################################################
##################################################################### This function is way too big, i fucked up ##########################################################################
def drawMap():
    global run, play, noPlanets, select, send, shipSpeed, firstFrame
    if run:
        #create objects
        if play:
            #Planets init
            for i in range(noPlanets):
                if i==0:
                    p[i] = GO.Planets(random.randint(-80,-70)/10,random.randint(-40,-30)/10, 0.7, 1)   #if this is the first planet declare and move to 2nd
                elif i==1:
                    p[i] = GO.Planets(random.randint(60,65)/10,random.randint(30,40)/10, 0.7, 2)   #if this is the second planet declare and move to 3rd
                elif i==2:
                    p[i] = GO.Planets(random.randint(-80,-70)/10,random.randint(30,40)/10, 0.7, 3)   #if this is the third planet declare and move to 4th
                elif i==3:
                    p[i] = GO.Planets(random.randint(60,65)/10,random.randint(-40,-30)/10, 0.7, 4)   #if this is the fourth planet declare and move to 5th
                else:
                    distanceCheck = True                                                                                    #used to loop until planets are not overlapped
                    while distanceCheck:
                        count = 0                                                                                       #used to check if planets overlap
                        p[i] = GO.Planets(random.randint(-80,65)/10,random.randint(-40,40)/10,random.randint(4,6)/10, 0)   #declare objects planets
                        for j in range(i):
                            dist = math.sqrt((p[i].x-p[j].x)**2+(p[i].y-p[j].y)**2)                  #calculate distance between two planets
                            if (dist<p[i].Rad+p[j].Rad+0.5):                        #check if distance is smaller than sum of radiuses (overlap)
                                break
                            else:                                       #if not overlap count++
                                count += 1
                        if count == i:                              #if curent created planet doesn't overlap with the rest 
                            distanceCheck = False                   #move to next planet
            for i in range(noPlanets):
                for j in range(noPlanets):
                    p[i].Distance[j][0] = math.sqrt((p[i].x-p[j].x)**2+(p[i].y-p[j].y)**2)
                    p[i].Distance[j][1] = j
            for k in range(noPlanets):
                for i in range(noPlanets-1):
                    for j in range(i+1, noPlanets):
                        if (p[k].Distance[i][0]>p[k].Distance[j][0]):
                            x, y = p[k].Distance[i]
                            p[k].Distance[i] = p[k].Distance[j]
                            p[k].Distance[j] = x, y
            #init players
            for i in range(5):
                pl[i] = GO.Players()
            for i in range(noShips):
                shipsAlive[i] = False
            play = False        #finish creating planets and players

        else:
            #Ship init
            if send:
                tempx, tempy = getMouseCoord()          #get click coords in opengl units
                for i in range(noPlanets):
                    if math.sqrt((tempx-p[i].x)**2+(tempy-p[i].y)**2)<=p[i].Rad:         #check if planet was clicked
                        for j in range(noPlanets):
                            if p[j].Selected and j == i:                   #check if planet(s) is(are) selected and if selected planet is also clicked planet
                                p[j].Selected = False
                            elif p[j].Selected and j != i:              
                                for k in range(noShips):
                                    if not shipsAlive[k]:       #check if current object is free for usage
                                        if int(p[j].Troops*amount/100) >= 1:    #check if we can send at least one troop
                                            s[k] = GO.Ships(p[j].x, p[j].y, p[i].x, p[i].y, i, j, p[j].Player, int(p[j].Troops*amount/100))     #if free, create object ship with selected and destination planets coords, planet no., player and troops to send
                                            shipsAlive[k] = True        #mark object as alive
                                            p[j].Selected = False       #after we send a ship deselect the planet
                                            p[j].Troops -= int(p[j].Troops*amount/100)
                                            if p[j].Player == 2:
                                                p[j].sent = True
                                            break           #we send only one ship per planet (for now)
                                        else:
                                            p[j].Selected = False #deselect w/o doing anything
                        break       #only one planet at a time can be RMB clicked
                send = False

        #draw objects
        glLoadIdentity()
        glTranslatef(0.0, 0.0, zoom_val)
        tempx, tempy = getMouseCoord()          #get click coords in opengl units
        for i in range(noPlanets):
            glEnable(GL_TEXTURE_2D)
            if p[i].Player == 1:
                glBindTexture(GL_TEXTURE_2D, texture[1])
            elif p[i].Player == 2:
                glBindTexture(GL_TEXTURE_2D, texture[2])
            elif p[i].Player == 3:
                glBindTexture(GL_TEXTURE_2D, texture[3])
            elif p[i].Player == 4:
                glBindTexture(GL_TEXTURE_2D, texture[4])
            else:
                glBindTexture(GL_TEXTURE_2D, texture[0])
            p[i].drawPlanet()       #draw planets
            glDisable(GL_TEXTURE_2D)
            if p[i].Troops < 10:
                drawText(p[i].x-0.05,p[i].y-0.15, str(int(p[i].Troops)))         #draw text for one character
            elif p[i].Troops < 100:
                drawText(p[i].x-0.15,p[i].y-0.15, str(int(p[i].Troops)))         #draw text for two characters
            else:
                drawText(p[i].x-0.25,p[i].y-0.15, str(int(p[i].Troops)))         #draw text for three characters
            if select == 1 and p[i].Player == 1 and math.sqrt((tempx-p[i].x)**2+(tempy-p[i].y)**2)<=p[i].Rad:               #check if planet was clicked
                p[i].Selected = True        #if it was clicked, select it
                select = 0
            elif select == 2 and p[i].Player == 1 and math.sqrt((tempx-p[i].x)**2+(tempy-p[i].y)**2)<=p[i].Rad:      #check if planet was clicked
                for j in range(noPlanets):          #if select == 2 select all planets
                    if p[j].Player == 1:            #that belong to player 1
                        p[j].Selected = True        
                        select = 0

        #ships movement
        movePerFps= clock.get_time()/800     #calculate ships speed for every player according to miliseconds per frame (not finished)!!!!!!!!!!!!!!!!!!!!
        for i in range(1,5):
            shipSpeed[i] = movePerFps*pl[i].Speed     #calculate ships speed for every player according to miliseconds per frame and research points spent in speed
            if pl[i].Kills >= 100:          #if kills >= 100
                pl[i].Res += 1              #add research point
                pl[i].Kills -= 100
        for i in range(noShips):
            if shipsAlive[i]:       #if ships exist
                if s[i].destY == s[i].y:            #if start and destination points define a parallel line with Y axis
                    if s[i].destX > s[i].x:
                        s[i].x += shipSpeed[s[i].Player]
                    else:
                        s[i].x -= shipSpeed[s[i].Player]
                elif s[i].destX == s[i].x:          #else if start and destination points define a parallel line with X axis
                    if s[i].destY > s[i].y:
                        s[i].y += shipSpeed[s[i].Player]
                    else:
                        s[i].y -= shipSpeed[s[i].Player]
                else:                               #if line is not parallel with any of the axis
                    Fi = math.acos((s[i].destX-s[i].x)/math.sqrt(math.pow(s[i].destX-s[i].x,2)+math.pow(s[i].destY-s[i].y,2))) #calculate angle
                    calculateX = math.cos(Fi)*shipSpeed[s[i].Player]                                        #calculate movement on X axis ( cos(Fi) = cateta alaturata/ipotenuza)
                    calculateY = math.sqrt(math.pow(shipSpeed[s[i].Player],2)-math.pow(calculateX,2))       #calculate movement on Y axis (Pitagora)
                    if s[i].destY < s[i].y:                 #if movement on Y axis is towards negative side
                        calculateY = (-1)*calculateY        #multiply result by -1
                    s[i].x += calculateX    #add movement to ship coords
                    s[i].y += calculateY
                s[i].drawShip(zoom_val, s[i].x, s[i].y)     #draw ships
                if math.sqrt((s[i].destX-s[i].x)**2+(s[i].destY-s[i].y)**2)<=p[s[i].Target].Rad:   #if ships reaches target
                    shipsAlive[i] = False                           #kill ship
                    p[s[i].Source].sent = False                     #for AI
                    if s[i].Player == p[s[i].Target].Player:        #if ship and planet have same owner
                        p[s[i].Target].Troops += s[i].Troops        #increase troops number
                    else:
                        tempTroops = p[s[i].Target].Troops
                        p[s[i].Target].Troops = p[s[i].Target].Troops*pl[p[s[i].Target].Player].Power - s[i].Troops*pl[s[i].Player].Power        #if not decrease troops
                        if p[s[i].Target].Troops < 0:               #if planet troop is below 0
                            p[s[i].Target].Troops = -1*p[s[i].Target].Troops
                            p[s[i].Target].Troops =  p[s[i].Target].Troops/pl[s[i].Player].Power    #divide by troops power
                            pl[s[i].Player].Kills += tempTroops         #add killed troops to attacker
                            pl[p[s[i].Target].Player].Kills += s[i].Troops - p[s[i].Target].Troops      #add killed troops to defender
                            p[s[i].Target].Player = s[i].Player     #ship owner conquers the planet
                            
                        else:
                            p[s[i].Target].Troops = p[s[i].Target].Troops/pl[p[s[i].Target].Player].Power   #divide by troops power
                            pl[s[i].Player].Kills += tempTroops - p[s[i].Target].Troops         #add killed troops to attacker
                            pl[p[s[i].Target].Player].Kills += s[i].Troops                      #add killed troops to defender
                         
        planetTroops()
        panel()
        if firstFrame<3:
            firstFrame += 1
        else:
            AI()

##################################################################### What AI thinks :D ##################################################################################################
def AI():
    for t in range(2,5):
        if pl[t].Res >= 1:                      #if unused research point
            if pl[t].Growth <= pl[t].Power:     #order is Growth > Power > Speed 
                pl[t].Growth += 1
                pl[t].Res -= 1
            elif pl[t].Power <= pl[t].Speed or pl[t].Speed == 3:
                pl[t].Power += 1
                pl[t].Res -= 1
            else:
                if pl[t].Speed < 3:
                    pl[t].Speed += 1
                    pl[t].Res -= 1
    for i in range(noPlanets):
            if p[i].Player != 0 and p[i].Player != 1 and not p[i].sent:  #if planet belongs to AI and there is no ship sent from it
                for j in range(1, noPlanets):
                    if p[p[i].Distance[j][1]].Player != p[i].Player:  #check if closest planet belongs to another player
                        for k in range(noShips):
                            if not shipsAlive[k]:       #check if current object is free for usage
                                if p[i].Troops >= 5:    #check if we can send at least one troop
                                    s[k] = GO.Ships(p[i].x, p[i].y, p[p[i].Distance[j][1]].x, p[p[i].Distance[j][1]].y, p[i].Distance[j][1], i, p[i].Player, int(p[i].Troops*0.7))     #if free, create object ship with selected and destination planets coords, planet no., player and troops to send
                                    shipsAlive[k] = True        #mark object as alive
                                    p[i].Troops -= int(p[i].Troops*0.7)  #substract sent troops from planet troops
                                    p[i].sent = True    #we've sent a ship from this planet
                                    break           #we send only one ship per planet (for now)
                        break


#################################################################### Maybe i'll just put this in another function #########################################################################
def planetTroops():
    global noPlanets
    for i in range(noPlanets):
        if p[i].Player == 0:
            p[i].Troops += p[i].Rad/100
        else:
            p[i].Troops += p[i].Rad/10 * pl[p[i].Player].Growth


'''
def PlaySong():
    global song
    pygame.mixer.music.load(song)
    pygame.mixer.music.play()    
'''


    
################################################################ Display a panel with research points and allocation #######################################################################
def panel():                #translate to minimum 7.5x, -4.0y and max 8.5x, 4.0y
    global select
    clock.tick(40)          #limit frames to 40
    glLoadIdentity()
    glTranslatef(0.0, 0.0, zoom_val)
    drawText(7.5, -4.5,'Fps: ' + str(int(clock.get_fps())))         #Render fps on screen
    drawText(7.5, 4.5, str(amount) + ' %')                          #Render percentage
    glColor3f(1, 1, 1)
    #Speed display
    drawText(7.5, 4.0, 'Speed')
    drawText(7.5, 3.5, str(pl[1].Speed))
    #Growth display
    drawText(7.5, 3.0, 'Growth')
    drawText(7.5, 2.5, str(pl[1].Growth))
    #Power display
    drawText(7.5, 2.0, 'Power')
    drawText(7.5, 1.5, str(pl[1].Power))
    #Research points display
    drawText(7.5, -3.0, 'Research')
    drawText(7.5, -3.3, 'Points')
    drawText(7.5, -3.8, str(pl[1].Res))
    #Checking for clicks on the panel buttons
    if select and pl[1].Res > 0:
        tempX, tempY = getMouseCoord()
        if math.sqrt((tempX-8.45)**2+(tempY-3.65)**2)<=0.15:        #checking in circle not rectangle because it's easier
            if pl[1].Speed < 3:
                pl[1].Speed += 1
                pl[1].Res -= 1
            select = False
        elif math.sqrt((tempX-8.45)**2+(tempY-2.65)**2)<=0.15:
            pl[1].Growth += 1
            pl[1].Res -= 1
            select = False
        elif math.sqrt((tempX-8.45)**2+(tempY-1.65)**2)<=0.15:
            pl[1].Power += 1
            pl[1].Res -= 1
            select = False

    #draw buttons
    glTranslatef(8.3, 4.5, 0.0)
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, texture[5])
    for i in range(3):
        glTranslatef(0.0, -1.0, 0.0)
        glBegin(GL_POLYGON)
        glTexCoord2f(0.0, 0.0)
        glVertex3f(0, 0, 0)
        glTexCoord2f(1.0, 0.0)
        glVertex3f(0.3, 0, 0)
        glTexCoord2f(1.0, 1.0)
        glVertex3f(0.3, 0.3, 0)
        glTexCoord2f(0.0, 1.0)
        glVertex3f(0, 0.3, 0)
        glEnd()
    glDisable(GL_TEXTURE_2D)

####################################################################### Initializing everything ##############################################################################################
def InitGl():
    global height, width, font
    pygame.init()
    pygame.display.set_caption("Space Fight")
    pygame.display.set_mode([width, height], pygame.OPENGL|pygame.DOUBLEBUF|pygame.FULLSCREEN)        #create window
    glClearColor(0.0, 0.0, 0.0, 0.0)	# This Will Clear The Background Color To Black
    glClearDepth(1.0)					# Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS)				# The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST)				# Enables Depth Testing
    glShadeModel(GL_SMOOTH)				# Enables Smooth Color Shading
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()					# Reset The Projection Matrix    
    gluPerspective(90, (width/height), 0.1, 50.0)
    glMatrixMode(GL_MODELVIEW)
    #create font
    font = pygame.font.Font (None, 30)  
    #load textures
    glEnable(GL_TEXTURE_2D)
    texture[0]=loadTexture('Data\Textures\Planet0.BMP')
    texture[1]=loadTexture('Data\Textures\Planet1.BMP')
    texture[2]=loadTexture('Data\Textures\Planet2.BMP')
    texture[3]=loadTexture('Data\Textures\Planet3.BMP')
    texture[4]=loadTexture('Data\Textures\Planet4.BMP')
    texture[5]=loadTexture('Data\Textures\Plus.BMP')


########################################################################## Putting it all together ##########################################################################################
def main():
    global run, play, spacekey, mouseX, mouseY, button, select, send, amount
    InitGl()
    running = True
    while running:
        key=pygame.key.get_pressed()                            #checking pressed keys
        if key[pygame.K_ESCAPE]:                                #if Escape is pressed quit
            running = False        
        if key[pygame.K_SPACE] and spacekey:                    #is Space key pressed? 
            run=True                                            #do stuff
            play=True
            spacekey=False
        elif not key[pygame.K_SPACE] and not spacekey:          #is Space key released? 
            spacekey = True
        if key[pygame.K_LSHIFT]:
            lshift = True
        else:
            lshift = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:          #is mouse button down?
                mouseX, mouseY = event.pos
                button = event.button                           #which button?
                if button == 1:                 #is LMB down?
                    if lshift:
                        select = 2
                    else:
                        select = 1
                elif button == 3:               #is RMB down?
                    send = True
                elif button == 4:
                    if amount <= 90:
                        amount += 10
                elif button == 5:
                    if amount >= 20:
                        amount -= 10
    
        glClearColor(0, 0, 0, 0)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        drawMap()
        pygame.display.flip()
        

main()