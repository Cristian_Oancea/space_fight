from OpenGL.GL import *
from OpenGL.GLU import *
import math
import random

class Planets:
    def __init__(self, x, y, Rad, Player):              #Coordinates and radius
        self.Player = Player
        self.x = x
        self.y = y
        self.Rad = Rad          #planet's radius
        self.sent = False       #was a ship sent from this planet? (for AI)
        if (self.Player == 0):                          #if planet is neutral
            self.Troops = random.randint(7,10)*Rad*5    #set random number of troops
        else:
            self.Troops = Rad*50   #else give fixed number of troops to eevery player
        self.Selected = False
        self.Distance = [[0 for temp1 in range(2)] for temp2 in range(30)]  #store distances from this planet to every planet in the system for easier computation (for AI)

    def drawPlanet(self):                       #Draw planet
        #sides = 32
        if self.Selected == True:
            glColor3f(1, 0, 0)
        else:
             glColor3f(1, 1, 1)
        glBegin(GL_POLYGON)
        glTexCoord2f(0.0, 0.0)
        glVertex3f(self.x-self.Rad, self.y-self.Rad, 0)
        glTexCoord2f(1.0, 0.0)
        glVertex3f(self.x+self.Rad, self.y-self.Rad, 0)
        glTexCoord2f(1.0, 1.0)
        glVertex3f(self.x+self.Rad, self.y+self.Rad, 0)
        glTexCoord2f(0.0, 1.0)
        glVertex3f(self.x-self.Rad, self.y+self.Rad, 0)
        glEnd()
        '''
        glBegin(GL_POLYGON)
        for i in range(sides):    
            dotX = self.Rad * math.cos(i*2*3.141592/sides) + self.x     #calculate dot position
            dotY = self.Rad * math.sin(i*2*3.141592/sides) + self.y     #on X axis and Y axis
            glColor3f(self.Red, self.Green, self.Blue)
            glVertex3f(dotX, dotY, 0)
        glEnd()
        if self.Selected:           #if planet is selected draw a circle around it
            glBegin(GL_LINE_LOOP)
            for i in range(sides):
                lineX = self.Rad * math.cos(i*2*3.141592/sides)*1.2 + self.x    #multiply by 1.2 to get a
                lineY = self.Rad * math.sin(i*2*3.141592/sides)*1.2 + self.y    #circle around the planet
                glVertex3f(lineX, lineY, 0)
            glEnd()
            '''

class Ships:
    def __init__(self, x, y, destX, destY, Target, Source, Player, Troops):                   #Coordinates from source planet and destination planet
        self.Player = Player        #which player does the ship belong to
        self.x = x
        self.y = y
        self.destX = destX          #ship's destination X coord
        self.destY = destY          #ship's destination Y coord
        self.Target = Target        #because i don't know how to use inheritance :(( stores the number of object planet
        self.Source = Source        #Same crap
        self.Troops = Troops        #number of troops carrying


    def drawShip(self, zoom, iVector, jVector):       #Vector and rotation
        Rot = math.degrees(math.acos(math.sqrt((self.destY-self.y)**2)/math.sqrt((self.destX-self.x)**2+(self.destY-self.y)**2)))     #calculate angle from curent vector and 0*i+1*j and transform to degrees from radians
        colors = ((0.5,0.5,0.5), (0,1,0), (1,1,1), (0,0,1), (1,0,1))
        glLoadIdentity()
        glTranslatef(iVector, jVector, zoom)
        if self.destX < self.x:             #needed to check if ship is moving left or right because variable Rot
            if self.destY > self.y:
                glRotatef(Rot, 0, 0, 1) 
            else:
                glRotatef(180-Rot, 0, 0, 1)
        else:                       #only goes to 180 degrees while glRotatef accepts as 1st param an angle up to 360 degrees
            if self.destY > self.y:
                glRotatef(360-Rot, 0, 0, 1)
            else:
                glRotatef(180+Rot, 0, 0, 1)
        glColor3f(*colors[self.Player])
        glBegin(GL_POLYGON)
        glVertex3f(0, 0.1, 0)
        glVertex3f(-0.1, -0.1, 0)
        glVertex3f(0.1, -0.1, 0)
        glEnd()

class Players:
    def __init__(self):
        self.Res = 1        #research points (set to one as initial value)
        self.Speed = 1      #ships speed modifier (set to 1 as initial value)
        self.Growth = 1     #Troops growth on the planets owned by this player (set to 1)
        self.Power = 1      #Troops power when attacking or defending (set to 0 aka default power)
        self.Kills = 0      #Enemy troops killed

